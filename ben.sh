#!/bin/bash

# Bench reodering stuff

max_diam_iters=${max_diam_iters:-10}
min_diam_diff=${min_diam_diff:-3}
iters=${iters:-5}
time_cmd=${time_cmd:-gtime}
diam_exe=${diam_exe:-./diam}
input_witness=${input_witness:-~/Downloads/VLG/inet}
input_test=${input_test:-"$input_witness"_}

function bench() {
    local input="$1"
    local tmp=$RANDOM.tmp
#   echo -e "Benchmark\titers:$iters\tinput:$input"
    local sum='( 0'
    for i in `seq 1 $iters`; do
        which $time_cmd >/dev/null
        if [[ $? -eq 0 ]]; then
            $time_cmd $diam_exe '-diam' $max_diam_iters $min_diam_diff <$input  >$tmp 2>&1
            local time=$(grep 'user' $tmp | cut -d ' ' -f 1 | sed 's/user//')
        else
            command time $diam_exe '-diam' $max_diam_iters $min_diam_diff <$input  >$tmp 2>&1
            local time=$(grep 'user' $tmp | tr -s ' ' | cut -d ' ' -f 4)
        fi
        sum="$sum + "$time
        sleep 1
    done
    rm $tmp
    echo "$sum ) / $iters" | bc -l
}

# bench $input_witness
# bench $input_test


function compare() {
    [[ $# -ne 2 ]] && exit 5
    local input_w="$1"
    local input_t="$2"
    local r1=$(bench $input_w)
    local r2=$(bench $input_t)
    echo -en 'Speedup of test over witness:  x'
    echo "$r1 / $r2" | bc -l
}

compare $input_witness $input_test
