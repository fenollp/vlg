#vlg • [Bitbucket](https://bitbucket.org/fenollp/vlg)

## Benchmark results

    iters=5 time_cmd=time input_witness=../inet input_test=../inet_ max_diam_iters=10  min_diam_diff=3  ./ben.sh
    Speedup of test over witness:  x3.04954043586334470200
    iters=5 time_cmd=time input_witness=../ip   input_test=../ip_   max_diam_iters=10  min_diam_diff=1  ./ben.sh
    Speedup of test over witness:  x1.05235742850421749504
    iters=5 time_cmd=time input_witness=../p2p  input_test=../p2p_  max_diam_iters=100 min_diam_diff=1  ./ben.sh
    Speedup of test over witness:  x1.17375617904417834493

