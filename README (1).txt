Clemence Magnien and Matthieu Latapy
September 2007
http://www-rp.lip6.fr/~magnien/Diameter
clemence.magnien@lip6.fr

This directory contains the data used for the program
computing bounds for the diameter of massive graphs
provided at the web page above.

You may freely use this data, provided you cite the paper:
 Fast Computation of Empirically Tight Bounds
  for the Diameter of Massive Graphs
 Clemence Magnien, Matthieu Latapy, and Michel Habib,
and the web page above.

* FORMAT *
**********

Each graph is provided as a single gzipped file, in the
following format:
the first line must be the number n of nodes; then comes
a series of lines of the form 'i j' meaning that node
'i' has degree 'j', and then a series of lines of the form
'u v' meaning that nodes 'u' and 'v' are linked together.
There *must* be no duplicate lines, and 'u v' also stands
for 'v u'. The nodes must be numbered from 0 to n-1. There
must be no loop 'u u'. The program makes basic verifications
but may crash or give wrong answers if the input is incorrect.

Example :
3
0 2
1 2
2 2
0 1
0 2
2 1
(3 nodes, thus numbered from 0 to 2, node 0 has degree 2, node 1
has degree 2, and node 2 has degree 2 too, and the links are 0 1,
0 2 and 2 1)

