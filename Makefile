all: diam

diam: diam.c prelim.c
	clang -O3 $< -o $@

test_ro: diam
	./diam -ro ~/Downloads/Diameter/coucou < pitigraph && cat ~/Downloads/Diameter/coucou
.PHONY: test_ro

clean:
	$(if $(wildcard diam), rm diam)

test:
	iters=5 time_cmd=time input_witness=../inet input_test=../inet_ max_diam_iters=10  min_diam_diff=3  ./ben.sh
	iters=5 time_cmd=time input_witness=../ip   input_test=../ip_   max_diam_iters=10  min_diam_diff=1  ./ben.sh
	iters=5 time_cmd=time input_witness=../p2p  input_test=../p2p_  max_diam_iters=100 min_diam_diff=1  ./ben.sh
